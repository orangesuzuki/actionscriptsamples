# AS3ライブラリサンプル集 #

AS3ライブラリを使ったサンプル集です。

## ThreadLibraryExamples

### 機能
- 画像を読み込み、表示・非表示を行うだけのシンプルなスレッドです。

### サンプル
![キャプチャ.PNG](https://bitbucket.org/repo/baqX7pz/images/1814718775-%E3%82%AD%E3%83%A3%E3%83%97%E3%83%81%E3%83%A3.PNG)

### リポジトリ
- https://orangesuzuki@bitbucket.org/orangesuzuki/actionscriptsamples.git

### ソースコード(zipファイル)
- https://bitbucket.org/orangesuzuki/actionscriptsamples/get/5a486b2b4af9.zip

### プロジェクトファイル一式
- https://bitbucket.org/orangesuzuki/actionscriptsamples/src/b2810f184b7ac407c97cf38cd9b136f8cefcb171/ThreadLibraryExamples/?at=master