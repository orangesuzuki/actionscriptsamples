package samples.photoalbum.thread.loader
{
	import flash.display.Bitmap;
	import flash.display.Loader;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.net.URLRequest;

	import org.libspark.thread.Thread;

	import samples.photoalbum.data.PhotoData;

	/**
	 * 単一のビットマップを読み込むスレッド
	 * Loaderを使い画像を読み込み後、PhotoDataインスタンスにビットマップをセットします。
	 * 
	 * Threadライブラリ公式のLoaderThreadクラスとは異なる読み込みをしたい場合のサンプル
	 * @author Katsushi.Suzuki
	 *
	 */
	public class LoadPhotoAssetThread extends Thread
	{

		//----------------------------------------------------------
		//
		//   Property 
		//
		//----------------------------------------------------------

		private var loader:Loader;
		private var photo:PhotoData;

		//----------------------------------------------------------
		//
		//   Constructor 
		//
		//----------------------------------------------------------

		public function LoadPhotoAssetThread(photo:PhotoData)
		{
			this.photo = photo;
			loader = new Loader();
		}

		override protected function run():void
		{
			events();

			// ロード開始
			var url:String = 'asset/thum/' + photo.id + '.jpg';
			loader.load(new URLRequest(url));
		}

		override protected function finalize():void
		{
			photo = null;
			if (loader)
			{
				loader = null;
			}
		}

		private function events():void
		{
			event(loader.contentLoaderInfo, Event.COMPLETE, completeHandler);
//			event(_loader, ProgressEvent.PROGRESS, progressHandler);
			event(loader.contentLoaderInfo, IOErrorEvent.IO_ERROR, ioErrorHandler);
//			event(_loader, SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);
		}

		private function ioErrorHandler(e:Event):void
		{
			//trace('error', photo.id);
		}

		private function completeHandler(e:Event):void
		{
			//trace(photo.id);
			// データにビットマップをセットします
			photo.bitmap = loader.content as Bitmap;
		}
	}
}
