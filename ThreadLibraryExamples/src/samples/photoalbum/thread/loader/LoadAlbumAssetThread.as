package samples.photoalbum.thread.loader
{
	import org.libspark.thread.Thread;
	import org.libspark.thread.utils.ParallelExecutor;
	import org.libspark.thread.utils.SerialExecutor;
	
	import samples.photoalbum.data.AlbumData;
	import samples.photoalbum.data.PhotoData;

	/**
	 * アセットを読み込むスレッド 
	 * @author Katsushi.Suzuki
	 * 
	 */	
	public class LoadAlbumAssetThread extends Thread
	{

		//----------------------------------------------------------
		//
		//   Property 
		//
		//----------------------------------------------------------

		private var album:AlbumData;

		//----------------------------------------------------------
		//
		//   Constructor 
		//
		//----------------------------------------------------------

		public function LoadAlbumAssetThread(album:AlbumData)
		{
			this.album = album;
		}

		/**
		 * スレッドの開始時に実行されます。
		 *
		 */
		override protected function run():void
		{
			trace('\tLoadAlbumAssetThread.run');
			// SerialExecutor の開始
//			var executor:SerialExecutor = new SerialExecutor();
			var executor:ParallelExecutor = new ParallelExecutor();
			for each (var photo:PhotoData in album.photoSet)
			{
				executor.addThread(new LoadPhotoAssetThread(photo));
			}
			executor.start();

			// SerialExecutor の終了を待つ
			executor.join();

			next(end);
		}

		/**
		 * スレッドの終了時に実行されます。
		 *
		 */
		override protected function finalize():void
		{
			trace('\tLoadAlbumAssetThread.finalize');
			album = null;
		}

		private function end():void
		{
			trace('\tLoadAlbumAssetThread.end');
			// 再び別の処理を実行したい場合はここで行う
		}
	}
}
