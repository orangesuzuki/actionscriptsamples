package samples.photoalbum.thread.view
{
	import flash.display.Bitmap;
	import flash.display.DisplayObjectContainer;

	import a24.tween.Tween24;

	import org.libspark.thread.Thread;

	import samples.photoalbum.data.AlbumData;
	import samples.photoalbum.data.PhotoData;

	/**
	 * アニメーションを制御するスレッド
	 * @author Katsushi.Suzuki
	 *
	 */
	public class AlbumViewerThread extends Thread
	{

		//----------------------------------------------------------
		//
		//   Property 
		//
		//----------------------------------------------------------

		private var album:AlbumData;
		private var container:DisplayObjectContainer;

		//----------------------------------------------------------
		//
		//   Constructor 
		//
		//----------------------------------------------------------

		public function AlbumViewerThread(container:DisplayObjectContainer, album:AlbumData)
		{
			this.container = container;
			this.album = album;
		}

		/**
		 * スレッドの開始時に実行されます。
		 *
		 */
		override protected function run():void
		{
			trace('\tAlbumViewerThread.run');
			// 表示アニメーション
			show();
			// 終了アニメーション
			next(hide);
		}

		/**
		 * スレッドの終了時に実行されます。
		 *
		 */
		override protected function finalize():void
		{
			trace('\tAlbumViewerThread.finalize');
			if (container)
			{
				container.removeChildren();
				container = null;				
			}
			album = null;
		}

		/**
		 * 表示アニメーション
		 *
		 */
		private function show():void
		{
			trace('\tAlbumViewerThread.show');
			// スレッドを待機
			wait();

			var twees:Array = [];
			var counter:int = 0;
			for each (var photo:PhotoData in album.photoSet)
			{
				var bitmap:Bitmap = photo.bitmap;

				var width:Number = bitmap.width + 10;
				var height:Number = bitmap.height + 10;
				var ix:int = 50;
				var iy:int = 50;
				bitmap.x = ix + (counter % 5) * width;
				bitmap.y = iy + int(counter / 5) * height;
				container.addChild(bitmap);

				twees.push(Tween24.serial(
						   Tween24.prop(bitmap).fadeOut(),
						   Tween24.wait(0.05 * counter++),
						   Tween24.tween(bitmap, 0.4).fadeIn()
						   ))
			}

			var showTween:Tween24 =
				Tween24.serial(
				// 表示アニメーション
				Tween24.parallel(twees),
				// 1秒待機します
				Tween24.wait(1),
				// スレッドを再開
				Tween24.func(notify)
				);
			showTween.play();
		}

		/**
		 * 終了アニメーション
		 *
		 */
		private function hide():void
		{
			trace('\tAlbumViewerThread.hide');
			// スレッドを待機
			wait();

			var counter:int = 0;
			var twees:Array = [];
			for each (var photo:PhotoData in album.photoSet)
			{
				var bitmap:Bitmap = photo.bitmap;
				twees.push(Tween24.serial(
						   Tween24.wait(0.05 * counter++),
						   Tween24.tween(bitmap, 0.2).fadeOut()
						   ))
			}

			var hideTween:Tween24 =
				Tween24.serial(
				// 終了アニメーション
				Tween24.parallel(twees),
				// スレッドを再開
				Tween24.func(notify)
				);
			hideTween.play();
		}
	}
}
