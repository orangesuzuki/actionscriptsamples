package samples.photoalbum.thread
{
	import flash.display.DisplayObjectContainer;

	import org.libspark.thread.Thread;
	import org.libspark.thread.utils.SerialExecutor;

	import samples.photoalbum.data.AlbumData;
	import samples.photoalbum.thread.loader.LoadAlbumAssetThread;
	import samples.photoalbum.thread.view.AlbumViewerThread;

	/**
	 * フォトアルバム機能全体のスレッド
	 *
	 * 1. アセットの読み込み
	 * 2. アセットの表示とアニメーション
	 * @author Katsushi.Suzuki
	 *
	 */
	public class PhotoAlbumThread extends Thread
	{

		//----------------------------------------------------------
		//
		//   Property 
		//
		//----------------------------------------------------------

		private var container:DisplayObjectContainer;
		private var albumData:AlbumData;

		//----------------------------------------------------------
		//
		//   Constructor 
		//
		//----------------------------------------------------------

		public function PhotoAlbumThread(container:DisplayObjectContainer)
		{
			this.container = container;
		}

		override protected function run():void
		{
			trace('PhotoAlbumThread.run');

			// データのセットアップ
			albumData = new AlbumData();

			// 複数のスレッドを順番に実行
			var executor:SerialExecutor = new SerialExecutor();
			// 1. アセットの読み込み
			executor.addThread(new LoadAlbumAssetThread(albumData));
			// 2. アセットの表示とアニメーション
			executor.addThread(new AlbumViewerThread(container, albumData));
			// 3. SerialExecutorを開始
			executor.start();
			// 4. SerialExecutor の終了を待つ
			executor.join();

			// スレッド実行後
			next(exit);
		}

		override protected function finalize():void
		{
			trace('PhotoAlbumThread.finalize');
			container = null;
			if (albumData)
			{
				albumData.dispose();
				albumData = null;
			}
		}

		private function exit():void
		{
			trace('PhotoAlbumThread.exit');
			// 再び別の処理を実行したい場合はここで行う
		}
	}
}
