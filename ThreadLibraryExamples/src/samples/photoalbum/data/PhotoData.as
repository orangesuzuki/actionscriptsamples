package samples.photoalbum.data
{
	import flash.display.Bitmap;

	/**
	 * 写真の単体データ
	 * @author Katsushi.Suzuki
	 *
	 */
	public class PhotoData
	{

		//----------------------------------------------------------
		//
		//   Property 
		//
		//----------------------------------------------------------

		public var bitmap:Bitmap;
		public var id:int;

		//----------------------------------------------------------
		//
		//   Constructor 
		//
		//----------------------------------------------------------

		public function PhotoData()
		{
		}

		public function dispose():void
		{
			if (bitmap)
			{
				bitmap.bitmapData.dispose();
				bitmap.bitmapData = null;
			}
			bitmap = null;
		}
	}
}
