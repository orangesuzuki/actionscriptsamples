package samples.photoalbum.data
{

	/**
	 * フォトアルバムのデータ 
	 * @author Katsushi.Suzuki
	 * 
	 */	
	public class AlbumData
	{

		//----------------------------------------------------------
		//
		//   Property 
		//
		//----------------------------------------------------------

		public var photoSet:Vector.<PhotoData>;

		//----------------------------------------------------------
		//
		//   Constructor 
		//
		//----------------------------------------------------------

		public function AlbumData()
		{
			photoSet = new Vector.<PhotoData>();

			var num:int = 20;
			for (var i:int = 1; i <= num; i++)
			{
				var photo:PhotoData = new PhotoData();
				photo.id = i;
				photoSet.push(photo);
			}
		}

		public function dispose():void
		{
			for each (var photo:PhotoData in photoSet)
			{
				photo.dispose();
			}
			photoSet = null;
		}
	}
}
