package
{
	import flash.display.Sprite;
	
	import a24.tween.Tween24;
	
	import org.libspark.thread.EnterFrameThreadExecutor;
	import org.libspark.thread.Thread;
	import org.libspark.thread.utils.SerialExecutor;
	
	import samples.photoalbum.thread.PhotoAlbumThread;

	/**
	 * スレッドライブラリのサンプル
	 * @author Katsushi.Suzuki
	 *
	 */
	[SWF(backgroundColor = "0xffffff", frameRate = "60", width = "1000", height = "640")]
	public class ThreadLibraryExamples extends Sprite
	{

		//----------------------------------------------------------
		//
		//   Constructor 
		//
		//----------------------------------------------------------

		public function ThreadLibraryExamples()
		{
			// スレッドの初期化
			Thread.initialize(new EnterFrameThreadExecutor());
			// フォトアルバム機能のテスト
			Tween24.func(photoAlbumTest).delay(0.5).play();
			
			// 複数フォトアルバムのテスト
			//Tween24.func(multiPhotoAlbumTest).delay(0.5).play();
		}

		/**
		 * フォトアルバムのテスト
		 * 
		 * 画像を読み込み、表示・非表示を行うだけのシンプルなスレッドです。
		 */
		private function photoAlbumTest():void
		{
			var t:PhotoAlbumThread = new PhotoAlbumThread(this);
			t.start();
		}
		
		/**
		 * 複数フォトアルバムのテスト
		 * 
		 * 単一のフォトアルバムを繰り返します。
		 */		
		private function multiPhotoAlbumTest():void
		{
			var executor:SerialExecutor = new SerialExecutor();
			executor.addThread( new PhotoAlbumThread(this));
			executor.addThread( new PhotoAlbumThread(this));
			executor.addThread( new PhotoAlbumThread(this));
			executor.start();
		}
	}
}
